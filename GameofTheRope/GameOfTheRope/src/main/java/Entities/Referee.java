package Entities;

import SharedMem.ContestantsBench;
import SharedMem.Playground;
import SharedMem.RefereeSite;
import SupportInf.GameResult;
import SupportInf.MatchResult;
import SupportInf.RefereeStatus;

public class Referee extends Thread{
	
	private RefereeStatus stat;

	private Playground playGround;
	private ContestantsBench contestantsBench;
	private RefereeSite	refereeSite;

	/*
	 * Construtor
	 */
	public Referee(ContestantsBench bench, Playground playground, RefereeSite refereeSite) {
		super();
		this.contestantsBench = bench;
		this.playGround = playground;
		this.refereeSite = refereeSite;
	}

	@Override
	public void run() {
			
		startMatch();
		
		do{
			annouceNewGame();
		
			do{
				callTrial();
				startTrial();
				
			}while (assertTrialDecision() == GameResult.None);
			
		
		}while (declareGameWinner() == MatchResult.None);	
	
		
		declareMatchWinner();
	}
	
	public void startMatch(){
		
		refereeSite.startMatch();
	}
	
	public void annouceNewGame(){
		
		refereeSite.annouceNewGame();
	}
	
	public void callTrial(){
		
		contestantsBench.callTrial();
		
	}
	
	public void startTrial(){
		
		playGround.startTrial();
	}
	
	public GameResult assertTrialDecision(){
		
		GameResult gameResult = playGround.assertTrialDecision();
		contestantsBench.assertTrialDecision();
		return gameResult;
	}
	
	public MatchResult declareGameWinner(){
		
		GameResult gameResult = playGround.declareGameWinner();
		MatchResult matchResult = refereeSite.declareGameWinner(gameResult);
		return matchResult;
		
	}

	
	public void declareMatchWinner(){
		contestantsBench.declareMatchWinner();
	}
	
	public void setRefereeStatus(RefereeStatus stat){
		
		this.stat = stat;
	}
	
	public RefereeStatus getRefereeStatus(){
		
		return this.stat;
	}
	
	
}