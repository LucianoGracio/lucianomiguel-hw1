package Entities;

import SharedMem.ContestantsBench;
import SharedMem.Playground;
import SupportInf.CoachStatus;

public class Coach extends Thread

{
	private int teamID;
	private CoachStatus stat;
	private boolean wonLastTrial;

	private Playground playGround;
	private ContestantsBench contestantsBench;


	/*
	 * Construtor
	 */
	public Coach(int teamID, ContestantsBench bench, Playground playground) {
		super();
		this.contestantsBench = bench;
		this.playGround = playground;
		this.teamID = teamID;
	}

	@Override
	public void run() {

		reviewNotesFirst();

		while (!contestantsBench.getEndOp()) {
			callContestants();
			informReferee();
			reviewNotes();

		}

	}

	public void reviewNotesFirst() {

		
		contestantsBench.reviewNotesFirst();
	}

	public void callContestants() {

		
		contestantsBench.callContestants();
		playGround.callContestants();

	}

	public void informReferee() {

		contestantsBench.informReferee();
		playGround.informReferee();

	}

	public void reviewNotes() {

		contestantsBench.reviewNotes();
	}

	public int getTeamID() {

		return teamID;
	}

	public void setStatus(CoachStatus stat) {

		this.stat = stat;
	}

	public CoachStatus getCoachStatus() {

		return this.stat;
	}

	public void setWonLastTrial(boolean wonLastTrial) {

		this.wonLastTrial = wonLastTrial;

	}

	public boolean getWonLastTrial() {

		return this.wonLastTrial;
	}
}