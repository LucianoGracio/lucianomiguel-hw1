package Entities;

import SharedMem.ContestantsBench;
import SharedMem.Playground;
import SupportInf.ContestantStatus;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Contestant extends Thread

{
        static final String[] nomes= {"Alberto","Joaquim","João","Francisco","Marcio","Pedro","Jose","Miguel","Sandro","Filipe","Luciano","Bráulio","Fernando","Anibal"};
        static final Logger LOGGER = Logger.getLogger( Contestant.class.getName() );
    
	/*
	 * Internal data
	 */
	private final int id;
        private final int teamID;
	private int strength;
        private final String nome;
	private ContestantStatus stat;

	private Playground playGround;
	private ContestantsBench contestantsBench;

	
	/*
	 * Construtor
	 */
	public Contestant(int ID, int teamID, ContestantsBench bench, Playground playground) {
		super();
		this.contestantsBench = bench;
		this.playGround = playground;
		this.id = ID;
		this.teamID = teamID;
                Random random = new Random();
		this.strength = random.nextInt(10) + 10;
                this.nome= nomes[random.nextInt(nomes.length)];
	}

	/*
	 * Life cycle
	 */
	@Override
   public void run()
   {
		
		seatDownFirst();
		
		while(!contestantsBench.getEndOp()){
		   
			   followCoachAdvice();
			   getReady();
			   pullTheRope();
			   amDone();
			   seatDown();
			 
		   
	   }
     
   }
	
	@Override
	public String toString() {
		return "Contestant [ID=" + id + ", teamID=" + teamID + ", strength="
				+ strength + ", stat=" + stat + ", nome="+ nome+ ", playGround=" + playGround
				+ ", contestantsBench=" + contestantsBench + "]";
	}

	public void seatDownFirst(){
		
		contestantsBench.seatDownFirst();
	}
	
	

	public void followCoachAdvice(){

		playGround.followCoachAdvice();

	}



	public void getReady() {

		playGround.getReady();

	}

	public void pullTheRope() {
		
		try {
			Thread.sleep((long) (Math.random() * 100 + 100));
		} catch (InterruptedException e) {
                    LOGGER.log(Level.SEVERE, "Error in Contestant.java while trying to pull the rope");
                    Thread.currentThread().interrupt();
                }
	}

	public void amDone(){
		
		playGround.amDone();

	}
	
	public void seatDown() {

		contestantsBench.seatDown();

	}
	
	public int getStrength(){
		
		return strength;
	}
	
	public void setStrength(int strength){
		
		if(strength>=0){
			this.strength = strength;
		}
		else{
			this.strength = 0;
		}
	}
	
	public void setStatus(ContestantStatus stat){
		
		this.stat = stat;
	}
	
	public ContestantStatus getContestantStatus(){
		
		return stat;
	}

	public int getTeamID() {
		
		return this.teamID;
	}

	public int getID() {
		
		return this.id;
	}	
}