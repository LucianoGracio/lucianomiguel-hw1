package SupportInf;

public enum GameResult {
	
	None, Team0ByKO, Team1ByKO, Team0, Team1, Draw;

}
