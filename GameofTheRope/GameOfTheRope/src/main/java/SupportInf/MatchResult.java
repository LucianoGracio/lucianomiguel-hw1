package SupportInf;

public enum MatchResult {
	None, Team0Wins, Team1Wins, Tie
}
