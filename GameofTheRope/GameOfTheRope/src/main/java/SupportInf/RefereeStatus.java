package SupportInf;

public enum RefereeStatus {
	StartOfTheMatch, StartOfAGame, TeamsReady, WaitForTrialConclusion, EndOfAGame, EndOfTheMatch;
}
