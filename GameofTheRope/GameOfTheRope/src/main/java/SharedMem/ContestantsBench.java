package SharedMem;

import Entities.Coach;
import Entities.Contestant;
import Entities.Referee;
import Main.ProbConst;
import SupportInf.CoachStatus;
import SupportInf.ContestantStatus;
import SupportInf.RefereeStatus;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ContestantsBench implements ProbConst{

        
        private static final Logger LOGGER = Logger.getLogger(ContestantsBench.class.getName() );
    
	/*
	 * Counters
	 */
	private int contestantsAtBench[]; // Number of contestants currently sitting on a TEAM's bench
	private int numCoachesReady;
	private int nNotesReviewed;

	/*
	 * threads references
	 */
	private Contestant[][] contestants; // Array with references to each team's contestants

	/*
	 * Identification of contestants that played in the last trial
	 */
	private int[][] contIDPlayedLastTrial;
	
	/*
	 * monitor control variables
	 */
	private boolean[][] contestantSelected;		 // controls if a certain contestant will play in the next trial
	private boolean[] teamAssembled; 			 // controls if the coach can notify the referee
	private boolean[] contestantsSit; 			 // controls if the coach can start reviewNotes
	private boolean[] trialCalled; 				 // controls if the coach can start callContestants
	private boolean coachesReady;
	private boolean notesReviewed;

	/*
	 * General Information Repository
	 */
	GeneralInformationRepository gIRepository;
	
	/*
	 * end of operations
	 */
	private boolean endOp;
	
	public ContestantsBench(GeneralInformationRepository gIRepository) {

		this.gIRepository=gIRepository;
		endOp = false;
		
		trialCalled = new boolean[T];
		contestantsAtBench = new int[T];
		teamAssembled = new boolean[T];
		contestantSelected = new boolean[T][C];
		contestantsSit = new boolean[T];
		contIDPlayedLastTrial = new int[T][R];

		/*
		 * Initialisation of access boolean variables as well as contestant's
		 * presence in the bench
		 */
		coachesReady = false;
		numCoachesReady = 0;
		nNotesReviewed = 0;
		notesReviewed = false;
		
		for (int i = 0; i < T; i++) {
			contestantsAtBench[i] = 0;
			teamAssembled[i] = false;
			contestantsSit[i] = false;
			trialCalled[i] = false;
			for (int j = 0; j < C; j++) {
				contestantSelected[i][j] = false;
			}
			for (int j = 0; j < R; j++){
				contIDPlayedLastTrial[i][j]=-1;
			}
		}
		contestants = new Contestant[T][C];

	}

	/*
	 * Sits a contestant of the given team (first time).
	 * 
	 * This function is called by each contestant in their initialisation.
	 */
	public synchronized void seatDownFirst() {

		int teamID, ID;

		teamID = ((Contestant) Thread.currentThread()).getTeamID();
		ID = ((Contestant) Thread.currentThread()).getID();

		contestants[teamID][ID] = (Contestant) Thread.currentThread();
		contestantsAtBench[teamID]++;
		contestants[teamID][ID].setStatus(ContestantStatus.SeatAtTheBench);
		
		gIRepository.setContStatus(teamID, ID, contestants[teamID][ID].getContestantStatus());
		gIRepository.setContStr(teamID, ID, contestants[teamID][ID].getStrength());
		while (!contestantSelected[teamID][ID]) {
			try {
				wait();
			} catch (InterruptedException e) {
                            LOGGER.log(Level.SEVERE, "Error in ContestantsBench while waiting for the coach to select teammates in seatDownFirst");
                            Thread.currentThread().interrupt();
			}
		}
		contestantSelected[teamID][ID] = false;

	}

	/*
	 * Coach waits for the referee to call a new trial
	 * 
	 * This function is called by each Coach when they're initialised
	 */
	public synchronized void reviewNotesFirst() {

		int teamID = ((Coach) Thread.currentThread()).getTeamID();

		((Coach) Thread.currentThread()).setWonLastTrial(false);
		
		((Coach) Thread.currentThread()).setStatus(CoachStatus.WaitForRefereeCommand);
		gIRepository.setCoachStatus(teamID, ((Coach) Thread.currentThread()).getCoachStatus());
		while (!trialCalled[teamID]) {

			try {
				wait();
			} catch (InterruptedException e) {
                            LOGGER.log(Level.SEVERE, "Error in ContestantsBench while waiting for the trial to be called by the referee");
                            Thread.currentThread().interrupt();
			}
		}
		
		trialCalled[teamID] = false;
	}

	/*
	 * Notifies the coaches to start assembling the respective teams
	 * 
	 * Updates Referee's status
	 * 
	 * Function called by Referee
	 */
	public synchronized void callTrial() {
		
		
		for (int i = 0; i < T; i++) {

			trialCalled[i] = true;
			
		}
		
		((Referee) Thread.currentThread()).setRefereeStatus(RefereeStatus.TeamsReady);

		gIRepository.incrementNumTrial();
		
		gIRepository.setRefStatus(((Referee) Thread.currentThread()).getRefereeStatus());
		gIRepository.write();
		notifyAll();

		while(!coachesReady){
			try {
				wait();
			} catch (InterruptedException e) {
                            LOGGER.log(Level.SEVERE, "Error in ContestantsBench while waiting for the coaches to select their respective teams");
                            Thread.currentThread().interrupt();
                        }
		}
		numCoachesReady = 0;
		coachesReady = false;
		
		
	}

	/*
	 * Picks a contestant from his team's bench accordingly to a predefined
	 * strategy.
	 * 
	 * This function is called by each coach.
	 * 
	 */
	public synchronized void callContestants() {
		((Coach) Thread.currentThread()).setStatus(CoachStatus.AssembleTeam);
		
		boolean wonLastTrial = ((Coach) Thread.currentThread()).getWonLastTrial();
		
		int teamID = ((Coach) Thread.currentThread()).getTeamID();
		gIRepository.setCoachStatus(teamID, ((Coach) Thread.currentThread()).getCoachStatus());
		gIRepository.write();

		
		// Initialisation of the structure to memorise the contestants picked
		
		int[] contSelected = new int[R];

		
		for (int i = 0; i < R; i++) {

			contSelected[i] = -1;
		}

		/*
		 * Strategy of the first coach
		 * 
		 * This coach selects the R strongest contestants to play in the next
		 * Trial
		 */
		if (teamID == 0) {

			int strongest = 0;
			int strongestId = 0;
			boolean Contained = false;
			
			for(int i = 0; i < R;i++){
				for(int j=0;j<C;j++){
					for(int k = i; k >= 0;k--){
						if(contestants[teamID][j].getID()==contSelected[k]){
							Contained=true;
						}
					}
					if(Contained==false){
						if(contestants[teamID][j].getStrength()>strongest){
							strongest=contestants[teamID][j].getStrength();
							strongestId=j;
						}
					}
					Contained=false;
				}
				contSelected[i]=strongestId;
				contIDPlayedLastTrial[teamID][i]=strongestId;
				strongestId = 0;
				strongest=0;
			}
		}
		
		
		/*
		 * Strategy of the second coach
		 * 
		 * This coach selects the R strongest contestants if he won the last trial
		 * otherwise he selects R random contestants, may the luck be on his side
		 */
		else {
			
			
			 if (!wonLastTrial){
			   int randomID;
				boolean Contained;
				for(int i=0;i<R;i++){
					do{
						Contained=false;
                                                Random random = new Random();
						randomID= random.nextInt(5);
						for (int j=i;j>=0;j--){
							if(randomID==contSelected[j]){
								Contained=true;
							}
						}
					}
					while(Contained);
					contSelected[i]=randomID;
					contIDPlayedLastTrial[teamID][i]=randomID;
				}		
			}
			else{	
				
				int strongest = 0;
				int strongestId = 0;
				boolean Contained = false;
				
				for(int i = 0; i < R;i++){
					for(int j=0;j<C;j++){
						for(int k = i; k >= 0;k--){
							if(contestants[teamID][j].getID()==contSelected[k]){
								Contained=true;
							}
						}
						if(Contained==false){
							if(contestants[teamID][j].getStrength()>strongest){
								strongest=contestants[teamID][j].getStrength();
								strongestId=j;
							}
						}
						Contained=false;
					}
					contSelected[i]=strongestId;
					contIDPlayedLastTrial[teamID][i]=strongestId;
					strongestId = 0;
					strongest=0;
				}
				
				
			}
		}
		for(int i=0;i<R;i++){
			
			contestantSelected[teamID][contSelected[i]]=true;
			
		}
		notifyAll();
			

		
	}

	
	/*
	 * This function notfies the referee that each coach is ready to move to the playground
	 * 
	 * This enables the referee to start a trial
	 */
	public synchronized void informReferee(){
		
		numCoachesReady++;
		
		if(numCoachesReady == T){
			coachesReady = true;
		}
		
		notifyAll();
	}
	
	
	/*
	 * This function puts the referee to sleep while waiting for both coaches to review their notes
	 */
	public synchronized void assertTrialDecision(){
		
		for(int i=0;i<T;i++){
			contestantsAtBench[i]=C-R;
		}
		while (!notesReviewed){
			
			try{
                            wait();
			}catch (InterruptedException e){
                            LOGGER.log(Level.SEVERE, "Error in ContestantsBench while waiting for the coaches to review their notes");
                            Thread.currentThread().interrupt();
                        }
		}
		notesReviewed = false;
		nNotesReviewed = 0;
	}
	
	/*
	 * Coach waits for all his contestants to sit
	 * Reviews his notes
	 * Waits for referee to call the next trial
	 */
	public synchronized void reviewNotes(){
		
		int teamID = ((Coach) Thread.currentThread()).getTeamID();

		((Coach) Thread.currentThread()).setStatus(CoachStatus.WaitForRefereeCommand);
		gIRepository.setCoachStatus(teamID, ((Coach) Thread.currentThread()).getCoachStatus());
		gIRepository.write();
		
		while(!contestantsSit[teamID]){
			
			try{
				wait(); 
			}catch (InterruptedException e){
                            LOGGER.log(Level.SEVERE, "Error in ContestantsBench while waiting for the contestants to sit");
                            Thread.currentThread().interrupt();
                        }		
		}
		
		// Updating the strength of each contestant
		boolean playedLastTrial;
		for(int i = 0; i< C; i++){
			playedLastTrial=false;
			for(int j=0;j<R;j++){
				if(contestants[teamID][i].getID()==contIDPlayedLastTrial[teamID][j]){
					playedLastTrial=true;
					contIDPlayedLastTrial[teamID][j]=-1;
				}
			}
			contestants[teamID][i].setStrength(playedLastTrial?contestants[teamID][i].getStrength()-1:contestants[teamID][i].getStrength()+1);
			gIRepository.setContStr(teamID, i, contestants[teamID][i].getStrength());
		}
		gIRepository.write();
		nNotesReviewed++;
		if(nNotesReviewed==T){
			notesReviewed=true;
		}
		notifyAll();
                
		
		while (!trialCalled[teamID]) {

			try {
				wait();
			} catch (InterruptedException e) {
                            LOGGER.log(Level.SEVERE, "Error in ContestantsBench while waiting for the referee to call the next trial");
                            Thread.currentThread().interrupt();
			}
		}

		trialCalled[teamID] = false;	
	}
	
	/*
	 * Contestant sits on the bench and notifies his coach
	 * waits to be selected in the next trial
	 */
	public synchronized void seatDown() {

		int teamID, ID;

		teamID = ((Contestant) Thread.currentThread()).getTeamID();
		ID = ((Contestant) Thread.currentThread()).getID();

		contestantsAtBench[teamID]++;
		contestants[teamID][ID].setStatus(ContestantStatus.SeatAtTheBench);
		gIRepository.setContStatus(teamID, ID, contestants[teamID][ID].getContestantStatus());
                gIRepository.removeContestantFromPosition(teamID, ID);
                gIRepository.write();
		if (contestantsAtBench[teamID] == C) {

			contestantsSit[teamID] = true;

		}
		notifyAll();
		while (!contestantSelected[teamID][ID]) {
			try {
				wait();
			} catch (InterruptedException e) {
                            LOGGER.log(Level.SEVERE, "Error while waiting for the contestant to be picked by his coach");
                            Thread.currentThread().interrupt();
			}
		}
               
		contestantSelected[teamID][ID] = false;
		//sinalizar num array que o contestant foi selecionado
		contestantsAtBench[teamID]--;
	}
	
	
	/*
	 *  This function notifies all the contestants and coaches that the game is over
	 */
	public synchronized void declareMatchWinner(){
		
		endOp = true;
		
		for (int i = 0; i < T; i++){
			contestantsSit[i] = true;
			trialCalled[i]= true;
			for (int j = 0; j < C; j++){
				contestantSelected[i][j] = true;
			}				
		}
		notifyAll();
		
		((Referee) Thread.currentThread()).setRefereeStatus(RefereeStatus.EndOfTheMatch);
		gIRepository.setRefStatus(RefereeStatus.EndOfTheMatch);
		gIRepository.write();
	}

	public synchronized boolean getEndOp() {
		
		return endOp;
	}
}
