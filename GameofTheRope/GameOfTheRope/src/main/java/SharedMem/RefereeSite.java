package SharedMem;

import Entities.Referee;
import Main.ProbConst;
import SupportInf.GameResult;
import SupportInf.MatchResult;
import SupportInf.RefereeStatus;


public class RefereeSite implements ProbConst{

	private int curGame;
	private GameResult[] gameResults;
	private MatchResult matchResult;
	
	/*
	 * General Information Repository
	 */
	GeneralInformationRepository gIRepository;
	
	
	public RefereeSite(GeneralInformationRepository gIRepository){
		
		this.gIRepository=gIRepository;
		
		curGame = 0;
		gameResults = new GameResult[G];
		matchResult = MatchResult.None;
		
	}
	
	/*
	 * This function sets Referee's status in the start of the Match
	 */
	public synchronized void startMatch(){
		((Referee) Thread.currentThread()).setRefereeStatus(RefereeStatus.StartOfTheMatch);
		gIRepository.setRefStatus(RefereeStatus.StartOfTheMatch);
	}
	
	/*
	 * This function starts a new game
	 * 
	 * This function is called in announceNewGame
	 */
	
	public synchronized void annouceNewGame(){
		
		((Referee) Thread.currentThread()).setRefereeStatus(RefereeStatus.StartOfAGame);
		gIRepository.setRefStatus(((Referee) Thread.currentThread()).getRefereeStatus());
	
		curGame++;	
		gIRepository.setGameNum(curGame);
		gIRepository.setRopePosition(0);
		gIRepository.resetNumTrial();
		gIRepository.writeHeader();
		gIRepository.write();
		
		
	}
	
	/*
	 * Function saves the score of the last game and uses it to check if the Match is over
	 */
	public MatchResult declareGameWinner(GameResult gameResult) {
		
		((Referee) Thread.currentThread()).setRefereeStatus(RefereeStatus.EndOfAGame);
		gIRepository.setRefStatus(RefereeStatus.EndOfAGame);
		gIRepository.writeEndGame(gameResult);
		int wins0 = 0,
			wins1 = 0;
		
		
		gameResults[curGame-1] = gameResult;
		
		if (curGame == G){
			
			for (int i = 0; i<G; i++){
				
				if ( gameResults[i] == GameResult.Team0 || gameResults[i] == GameResult.Team0ByKO){
					
					wins0++;
				}
				if ( gameResults[i] == GameResult.Team1 || gameResults[i] == GameResult.Team1ByKO){
					
					wins1++;
				}
			}
			if (wins0 > wins1){
				
				matchResult = MatchResult.Team0Wins;
			}
			else if (wins0 < wins1){
				matchResult = MatchResult.Team1Wins;
			}
			else{
				matchResult = MatchResult.Tie;
			}
			gIRepository.writeEndMatch(matchResult);
			return matchResult;
		}

		return MatchResult.None;
	}	
}
