package SharedMem;

import genclass.GenericIO;
import genclass.TextFile;
import Main.ProbConst;
import SupportInf.CoachStatus;
import SupportInf.ContestantStatus;
import SupportInf.GameResult;
import SupportInf.MatchResult;
import SupportInf.RefereeStatus;

public class GeneralInformationRepository implements ProbConst {

	private int[][] contStr;
	private String[][] contStatus;
	private int[][] contIDinPosition;
	private String[] coachStatus;
	private String refStatus;
	private int numTrial;
	private int gameNum;
	private int ropePosition;
	private int winsTeam0;
	private int winsTeam1;

	String fileName;
	TextFile log;

	public GeneralInformationRepository(String fileName) {

		contStr = new int[T][C];
		contStatus = new String[T][C];
		coachStatus = new String[T];
		numTrial = 0;
		gameNum = 0;
		ropePosition = 0;
		contIDinPosition = new int[T][R];
		for (int i = 0; i < T; i++) {
			coachStatus[i]="WFRC";
			for (int j = 0; j < R; j++) {
				contIDinPosition[i][j] = -1;
			}
			for (int j = 0; j < C; j++) {
				contStatus[i][j]="SAB";
			}
		}

		/*
		 * initialisation of the logging file
		 */
		if ((fileName != null) && !("".equals(fileName))){
		
			this.fileName = fileName;
		}
		
		writeFirst();
	}

	private void writeFirst() {
		
		log = new TextFile();

		if (!log.openForWriting(".", fileName)) {
			GenericIO.writelnString("File creation unsuccessful: "+ fileName);
			System.exit(1);
		}
                String string = String.format("%30s %s"," ","Game of the Rope - Description of the internal state");
                log.writelnString(string);
		log.writelnString();
		if (!log.close()) {
			GenericIO.writelnString("File close operation unsuccessful: "+ fileName);
			System.exit(1);
		}
	}

	public synchronized void setContIDinPosition(int teamID, int ID) {
		
		for(int i = 0; i < R; i++){
			
			if(contIDinPosition[teamID][i]==-1){
				contIDinPosition[teamID][i] = ID+1;
				return;
			}
		}
	}

        public synchronized void removeContestantFromPosition(int teamID,int ID){
                for (int j = 0; j < R; j++) {
                    if((contIDinPosition[teamID][j]-1) == ID){
                        contIDinPosition[teamID][j] = -1;
                        return;
                    }
                }
        }

	public synchronized void setContStr(int teamID, int ID, int str) {

		contStr[teamID][ID] = str;
	}

	public synchronized void setContStatus(int teamID, int ID,
			ContestantStatus contStatus) {
		if (contStatus == ContestantStatus.SeatAtTheBench) {
			this.contStatus[teamID][ID] = "SAB";
		}
		if (contStatus == ContestantStatus.StandInPosition) {
			this.contStatus[teamID][ID] = "SIP";
		}
		if (contStatus == ContestantStatus.DoYourBest) {
			this.contStatus[teamID][ID] = "DYB";
		}
	}

	public synchronized void setCoachStatus(int teamID, CoachStatus coachStatus) {
		if (coachStatus == CoachStatus.AssembleTeam) {
			this.coachStatus[teamID] = "ASTM";
		}
		if (coachStatus == CoachStatus.WaitForRefereeCommand) {
			this.coachStatus[teamID] = "WFRC";
		}
		if (coachStatus == CoachStatus.WatchTrial) {
			this.coachStatus[teamID] = "WCTL";
		}

	}

	public synchronized void setRefStatus(RefereeStatus refStatus) {

		if (refStatus == RefereeStatus.EndOfAGame) {
			this.refStatus = "EOG";
		}
		if (refStatus == RefereeStatus.EndOfTheMatch) {
			this.refStatus = "EOM";
		}
		if (refStatus == RefereeStatus.StartOfAGame) {
			this.refStatus = "SOG";
		}
		if (refStatus == RefereeStatus.StartOfTheMatch) {
			this.refStatus = "SOM";
		}
		if (refStatus == RefereeStatus.TeamsReady) {
			this.refStatus = "TSR";
		}
		if (refStatus == RefereeStatus.WaitForTrialConclusion) {
			this.refStatus = "WTC";
		}
	}

	public synchronized void resetNumTrial() {

		numTrial = 0;
	}
	
	public synchronized void incrementNumTrial(){
		numTrial++;
	}

	public synchronized void setGameNum(int gameNum) {

		this.gameNum = gameNum;
	}

	public synchronized void setRopePosition(int ropePosition) {

		this.ropePosition = ropePosition;
	}

	public synchronized void writeHeader() {

		log = new TextFile();
		if (!log.openForAppending(".", fileName)) {
			GenericIO.writelnString("File creation unsuccessful: "+ fileName);
			System.exit(1);
		}
		log.writelnString("Game " + gameNum );
		log.writelnString("Ref Coa 1 Cont 1 Cont 2 Cont 3 Cont 4 Cont 5 Coa 2 Cont 1 Cont 2 Cont 3 Cont 4 Cont 5     Trial");
		log.writelnString("Sta Stat  Sta SG Sta SG Sta SG Sta SG Sta SG Stat  Sta SG Sta SG Sta SG Sta SG Sta SG 3 2 1 . 1 2 3 NB PS");
		if (!log.close()) {
			GenericIO.writelnString("File close operation unsuccessful: "+ fileName);
			System.exit(1);
		}

	}

	public synchronized void write() {

		log = new TextFile();
		if (!log.openForAppending(".", fileName)) {
			GenericIO.writelnString("File creation unsuccessful: "+ fileName);
			System.exit(1);
		}
		
		String string = String.format("%3s %4s  %3s %2d %3s %2d %3s %2d %3s %2d %3s %2d %4s  %3s %2d %3s %2d" +
				" %3s %2d %3s %2d %3s %2d %1s %1s %1s . %1s %1s %1s %2d %2d",
				refStatus,coachStatus[0], contStatus[0][0], contStr[0][0], contStatus[0][1],
				contStr[0][1], contStatus[0][2], contStr[0][2], contStatus[0][3], contStr[0][3],
				contStatus[0][4], contStr[0][4], coachStatus[1], contStatus[1][0], contStr[1][0],
				contStatus[1][1], contStr[1][1], contStatus[1][2], contStr[1][2], contStatus[1][3],
				contStr[1][3], contStatus[1][4], contStr[1][4],
				String.valueOf(contIDinPosition[0][0] == -1 ? "-" : contIDinPosition[0][0]),
				String.valueOf(contIDinPosition[0][1] == -1 ? "-" : contIDinPosition[0][1]),
				String.valueOf(contIDinPosition[0][2] == -1 ? "-" : contIDinPosition[0][2]), 
				String.valueOf(contIDinPosition[1][2] == -1 ? "-" : contIDinPosition[1][2]),
				String.valueOf(contIDinPosition[1][1] == -1 ? "-" : contIDinPosition[1][1]),
				String.valueOf(contIDinPosition[1][0] == -1 ? "-" : contIDinPosition[1][0]),
				numTrial, ropePosition);

		log.writelnString(string);
		
		if (!log.close()) {
			GenericIO.writelnString("File close operation unsuccessful: "+ fileName);
			System.exit(1);
		}

	}

	public synchronized void writeEndGame(GameResult gameResult) {
		
		log = new TextFile();
		if (!log.openForAppending(".", fileName)) {
			GenericIO.writelnString("File creation unsuccessful: "+ fileName);
			System.exit(1);
		}
		log.writelnString();
		if (gameResult == GameResult.Draw) {
			log.writelnString("Game " + gameNum + " was a draw");
		}
		if (gameResult == GameResult.Team1) {
			log.writelnString("Game " + gameNum + " was won by team 1 by points");
			winsTeam1++;
		}
		if (gameResult == GameResult.Team1ByKO) {
			log.writelnString("Game " + gameNum + " was won by team 1 by knock out in "+numTrial+" trials");
			winsTeam1++;
		}
		if (gameResult == GameResult.Team0) {
			log.writelnString("Game " + gameNum + " was won by team 1 by points");
			winsTeam0++;
		}
		if (gameResult == GameResult.Team0ByKO) {
			log.writelnString("Game " + gameNum + " was won by team 1 by knock out in "+numTrial+" trials");
			winsTeam0++;
		}
		log.writelnString();
		if (!log.close()) {
			GenericIO.writelnString("File close operation unsuccessful: "+ fileName);
			System.exit(1);
		}
	}

	public synchronized void writeEndMatch(MatchResult matchResult) {
		
		log = new TextFile();
		if (!log.openForAppending(".", fileName)) {
			GenericIO.writelnString("File creation unsuccessful: "+ fileName);
			System.exit(1);
		}
		log.writelnString();
		if (matchResult == MatchResult.Team0Wins) {
			log.writelnString("Match was won by team 0 ("+winsTeam0+"-"+winsTeam1+")");
		}
		if (matchResult == MatchResult.Team1Wins) {
			log.writelnString("Match was won by team 1 ("+winsTeam0+"-"+winsTeam1+")");
		}
		if (matchResult == MatchResult.Tie) {
			log.writelnString("Match was a draw");
		}
		log.writelnString();
		if (!log.close()) {
			GenericIO.writelnString("File close operation unsuccessful: "+ fileName);
			System.exit(1);
		}
		
		
	}
}
