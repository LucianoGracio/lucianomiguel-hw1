package SharedMem;
import Entities.Coach;
import Entities.Contestant;
import Entities.Referee;
import Main.ProbConst;
import SupportInf.CoachStatus;
import SupportInf.ContestantStatus;
import SupportInf.GameResult;
import SupportInf.RefereeStatus;

public class Playground implements ProbConst{


	private int contestantsInPosition[]; 	// Number of contestants currently in position
	private int teamStrength[]; 			// Strength of each team
	private int ropePosition;				// position of the rope at a given moment
	private int trialNum;					// number of the current trial
	private int contestantsPullingRope;		// Number of contestants pulling the rope
	private GameResult gameResult;			// result of the last Game
	
	/*
	 * monitor control variables
	 */
	private boolean contDismissed[][];
	private boolean coachDismissed[];
	private boolean teamReady[];
	private boolean trialStarted;
	private boolean everyoneFinished;
	
	
	/*
	 * General Information Repository
	 */
	GeneralInformationRepository gIRepository;

	public Playground(GeneralInformationRepository GIRepository) {
		
		this.gIRepository=GIRepository;
		
		contDismissed = new boolean[T][C];
		coachDismissed = new boolean[T];		
		trialStarted = false;
		everyoneFinished = false;
		teamReady = new boolean[T];
		contestantsInPosition = new int[T];
		teamStrength = new int[T];
		ropePosition = 0;
		contestantsPullingRope = 0;
		trialNum = 1;
		gameResult = GameResult.None;
		
		for (int i = 0; i < T; i++){
			
			coachDismissed[i] = false;
			teamReady[i] = false;
			contestantsInPosition[i] = 0;
			teamStrength[i] = 0;
			
			for (int j = 0; j < C; j++){
				
				contDismissed[i][j] = false;
			}
		}
		

	}
	
	
	/*
	 * Coach waits for the team to be ready
	 * Then notifies the referee that coach's team is ready
	 */
	public synchronized void callContestants(){
		
		int teamID = ((Coach) Thread.currentThread()).getTeamID();

		
		while(!teamReady[teamID]){
				
			try{
				wait();
			}catch (InterruptedException e){}
		}
		
		notifyAll();
	}
	
	/*
	 * Puts a contestant in his end of the rope
	 * 
	 * updates team strength
	 * 
	 * The last contestant to be in position notifies his coach
	 * 
	 * Function called by Contestant
	 */
	public synchronized void followCoachAdvice() {

		((Contestant) Thread.currentThread()).setStatus(ContestantStatus.StandInPosition);
		
		int teamID = ((Contestant) Thread.currentThread()).getTeamID();
		int ID = ((Contestant) Thread.currentThread()).getID();
		
		gIRepository.setContStatus(teamID,ID,((Contestant) Thread.currentThread()).getContestantStatus() );
		gIRepository.setContIDinPosition(teamID, ID);
		
		teamStrength[teamID] += ((Contestant) Thread.currentThread()).getStrength();
		contestantsInPosition[teamID]++;

		if (this.contestantsInPosition[teamID] == R) {
			teamReady[teamID] = true;
		}
		notifyAll();

		gIRepository.write();
		
		while (!trialStarted){			//Contestant waits for the trial to start
			
			try{
				wait();
			}catch (InterruptedException e){}
		}
	
	}
	
	/*
	 * Function informs referee
	 */
	public synchronized void informReferee(){
		((Coach) Thread.currentThread()).setStatus(CoachStatus.WatchTrial);
		int teamID = ((Coach) Thread.currentThread()).getTeamID();
		gIRepository.setCoachStatus(teamID, ((Coach) Thread.currentThread()).getCoachStatus());
		gIRepository.write();
		while(!coachDismissed[teamID]){
			try{
				wait();
			}catch(InterruptedException e){
			}
		}
		coachDismissed[teamID] = false;
	}
	
	/*
	 * Function updates trial number
	 * Changes status of referee
	 * 
	 */
	public synchronized void startTrial(){
		
		for (int i = 0; i < T; i++){
			for (int j = 0; j < C; j++){
				contDismissed[i][j] = false;
			}
		}
		
		trialStarted=true;
		((Referee) Thread.currentThread()).setRefereeStatus(RefereeStatus.WaitForTrialConclusion);
		gIRepository.setRefStatus(((Referee) Thread.currentThread()).getRefereeStatus());
		gIRepository.write();
		notifyAll();
		while(!everyoneFinished){
			try{
				wait();
			}catch(InterruptedException e){}
		}	
	}

	/*
	 * Function changes the status of the given contestant
	 */
	public synchronized void getReady(){
		
		((Contestant)Thread.currentThread()).setStatus(ContestantStatus.DoYourBest);
		int teamID = ((Contestant)Thread.currentThread()).getTeamID();
		int ID = ((Contestant)Thread.currentThread()).getID();
		
		gIRepository.setContStatus(teamID, ID,((Contestant)Thread.currentThread()).getContestantStatus());
		gIRepository.write();
	}
	
	/*
	 * Function notifies the referee that this contestant is finished pulling the rope
	 * 
	 * Then he waits for the referee to announce the end of the trial
	 */
	public synchronized void amDone(){
		
		int teamID = ((Contestant)Thread.currentThread()).getTeamID();
		int ID = ((Contestant)Thread.currentThread()).getID();
		
		contestantsPullingRope++;
		
		if (contestantsPullingRope == 2*R){
			everyoneFinished = true;
		}
		notifyAll();
		
		while (!contDismissed[teamID][ID]){
			try{
				wait();
			}catch(InterruptedException e){}
		}
		contDismissed[teamID][ID] = false;
               
	}
	
	/*
	 * this function updates the rope position and checks if the current game is finished,returning true if so
	 * 
	 * it is called by the referee in assertTrialDecisionFunction
	 */
	public synchronized GameResult assertTrialDecision(){
		
		boolean gameFinished = false;
		trialNum++;

		/*
		 * Trial Information
		 */
		if (teamStrength[0] > teamStrength[1]){
			
			ropePosition--;
		}
		else if (teamStrength[0] < teamStrength[1]){
			
			ropePosition++;
		}
		else{		
		}
		gIRepository.setRopePosition(ropePosition);
		
		/*
		 * Game Information
		 */
		if (ropePosition == KO){
			
			gameResult = GameResult.Team1ByKO;
			gameFinished = true;
		}
		
		else if (ropePosition == -KO){
			
			gameResult = GameResult.Team0ByKO;
			gameFinished = true;
		}
		
		if (trialNum == K+1){
			
			if (ropePosition < 0){
				
				gameResult = GameResult.Team0;
			}
			
			else if (ropePosition > 0){
				
				gameResult = GameResult.Team1;
			}
			
			else{
				
				gameResult = GameResult.Draw;
			}
						
			gameFinished =  true;			
		}
		
		
		
		
		for (int i = 0; i < T; i++){
			
			teamStrength[i] = 0;
			contestantsInPosition[i] = 0;
			teamReady[i] = false;
		}
		
		contestantsPullingRope = 0;
		trialStarted = false;
		everyoneFinished = false;
		
		/*
		 * signal the end of the trial to all coaches and contestants
		 */
		for (int i = 0; i < T; i++){
			
			coachDismissed[i] = true;
			
			for(int j = 0; j < C; j++){
				contDismissed[i][j] = true;
			}
		}
		
		notifyAll();
		
		/*
		 * Resetting monitor control variables
		 */
		if (gameFinished){
			gIRepository.write();
			ropePosition = 0;
			trialNum = 1;
		}
		else{
			gameResult = GameResult.None;
		}
		
		return gameResult;		
	}


	public GameResult declareGameWinner() {
		return gameResult;
	}	
}