package Main;

import genclass.GenericIO;
import Entities.Coach;
import Entities.Contestant;
import Entities.Referee;
import SharedMem.ContestantsBench;
import SharedMem.GeneralInformationRepository;
import SharedMem.Playground;
import SharedMem.RefereeSite;

public class Main implements ProbConst {

	public static void main(String[] args) {

		
		/*
		 * Thread's declaration
		 */
		Contestant[] cont = new Contestant[C * T];
		Coach[] coach = new Coach[T];
		Referee referee;
		
		
		/*
		 * declaration and initialization of the GeneralInformationRepository
		 */
		GeneralInformationRepository gIRepository = new GeneralInformationRepository("logging.txt");

		/*
		 * declaration and initialization of the Shared memory
		 */
		ContestantsBench bench = new ContestantsBench(gIRepository);
		Playground playground = new Playground(gIRepository);
		RefereeSite refereeSite = new RefereeSite(gIRepository);
		

		

		/*
		 * Initialization of the threads
		 */
		for (int i = 0; i < T*C; i++) {
			
			cont[i] = new Contestant(i%C, (i<C)?0:1, bench, playground);
		}
		
		for (int i = 0; i < T; i++) {

			coach[i] = new Coach(i, bench, playground);
		}
		
		referee = new Referee(bench, playground, refereeSite);

		/*
		 * Start of the Match
		 */
		for (int i = 0; i <T * C; i++) {
			cont[i].start();
			GenericIO.writelnString("Contestant thread " + i%C + " has started.");
		}
		for (int i = 0; i < T; i++) {
			coach[i].start();
			GenericIO.writelnString("Coach thread " + i + " has started.");
		}
		referee.start();
		GenericIO.writelnString("Referee thread has started.");
		
		/*
		 * Wait for the Match to finish
		 */
		for (int i = 0; i < C * T; i++) {
			try {
				cont[i].join();
			} catch (InterruptedException e) {
			}
			GenericIO.writelnString("Contestant thread " + i%C + " has ended.");
		}

		/* wait for the end of the simulation */

		for (int i = 0; i < T; i++) {
			try {
				coach[i].join();
			} catch (InterruptedException e) {
			}
			GenericIO.writelnString("Coach thread " + i + " has ended.");
		}

		try {
			referee.join();
		} catch (InterruptedException e) {
		}
		GenericIO.writelnString("Referee has ended.");

	}
}
