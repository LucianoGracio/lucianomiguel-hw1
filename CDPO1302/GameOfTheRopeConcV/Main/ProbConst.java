package Main;

public interface ProbConst {


    public final int T = 2,		//Number of teams
                     C = 5,		//Number of contestants per team
                     R = 3,		//Number of contestants playing each trial per team
                     K = 6,		//Maximum trials per game
                     G = 3,		//Number of games per match
                     M = 1,		//Number of matches
                     KO = 4;		//Rope position for knock out victory condition
					 
	
}
