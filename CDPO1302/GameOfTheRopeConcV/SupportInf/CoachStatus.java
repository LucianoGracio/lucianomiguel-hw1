package SupportInf;

public enum CoachStatus {
	AssembleTeam, WatchTrial, WaitForRefereeCommand
}
