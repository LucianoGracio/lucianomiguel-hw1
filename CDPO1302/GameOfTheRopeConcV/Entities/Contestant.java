package Entities;

import SharedMem.ContestantsBench;
import SharedMem.Playground;
import SupportInf.ContestantStatus;

public class Contestant extends Thread

{
	/*
	 * Internal data
	 */
	private final int ID, teamID;
	private int strength;
	private ContestantStatus stat;

	private Playground playGround;
	private ContestantsBench contestantsBench;

	
	/*
	 * Construtor
	 */
	public Contestant(int ID, int teamID, ContestantsBench bench, Playground playground) {
		super();
		this.contestantsBench = bench;
		this.playGround = playground;
		this.ID = ID;
		this.teamID = teamID;
		this.strength = (int) (10*Math.random() + 10);
	}

	/*
	 * Life cycle
	 */
	@Override
   public void run()
   {
		
		seatDownFirst();
		
		while(!contestantsBench.getEndOp()){
		   
			   followCoachAdvice();
			   getReady();
			   pullTheRope();
			   amDone();
			   seatDown();
			 
		   
	   }
     
   }
	
	@Override
	public String toString() {
		return "Contestant [ID=" + ID + ", teamID=" + teamID + ", strength="
				+ strength + ", stat=" + stat + ", playGround=" + playGround
				+ ", contestantsBench=" + contestantsBench + "]";
	}

	public void seatDownFirst(){
		
		contestantsBench.seatDownFirst();
	}
	
	

	public void followCoachAdvice(){

		playGround.followCoachAdvice();

	}



	public void getReady() {

		playGround.getReady();

	}

	public void pullTheRope() {
		
		try {
			Thread.sleep((long) (Math.random() * 100 + 100));
		} catch (InterruptedException e) {}
	}

	public void amDone(){
		
		playGround.amDone();

	}
	
	public void seatDown() {

		contestantsBench.seatDown();

	}
	
	public int getStrength(){
		
		return strength;
	}
	
	public void setStrength(int strength){
		
		if(strength>=0){
			this.strength = strength;
		}
		else{
			strength = 0;
		}
	}
	
	public void setStatus(ContestantStatus stat){
		
		this.stat = stat;
	}
	
	public ContestantStatus getContestantStatus(){
		
		return stat;
	}

	public int getTeamID() {
		
		return this.teamID;
	}

	public int getID() {
		
		return this.ID;
	}	
}